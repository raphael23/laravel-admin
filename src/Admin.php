<?php
namespace Phent\Admin;

use Illuminate\Config\Repository;
use Illuminate\Session\SessionManager;

class Admin{
    protected  $session;
    protected $config;

    public function __construct(SessionManager $session, Repository $config)
    {
        $this->config=$config;
        $this->session = $session;
    }

    public  function test_rtn($msg = ''){
        $config_arr = $this->config->get('admin.options');
        return $msg.'------from your custom develop package!';
    }
}
